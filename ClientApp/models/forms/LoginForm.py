from django import forms

class LoginForm(forms.Form):
    mail = forms.CharField(label='Adresse mail', max_length=50)
    password = forms.CharField(label='Mot de passe', max_length=15)