from django.core import serializers
from ClientApp.business.AnnouncementBusiness import AnnouncementBusiness
from django.http.response import HttpResponse
from ClientApp.business.OrganizationBusiness import OrganizationBusiness
import json



def getAnnouncementsOfOrganization(request, *args, **kwargs):
    organization = OrganizationBusiness.getOrganizationById(request.GET['id'])
    if not organization: 
        return HttpResponse([], mimetype="application/json")
    json_answer = serializers.serialize('json', AnnouncementBusiness.getAnnouncementsOfOrganization(organization))
    return HttpResponse(json_answer, mimetype="application/json", status=200)