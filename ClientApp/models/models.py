# -*- coding: utf8 -*-
from django.db import models


class Module(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=30)
    displayName = models.CharField(max_length=30)
    description = models.CharField(max_length=150)
    
    class Meta:
        app_label = 'ClientApp'
    
class Organization(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    corporateName = models.CharField(max_length=50)
    activity = models.CharField(max_length=100)
    city = models.CharField(max_length=30)
    mail = models.EmailField(max_length=50)
    phone = models.IntegerField()
    creationDate = models.DateField()
    modules = models.ManyToManyField(Module, through='OrganizationModule')
    
    class Meta:
        app_label = 'ClientApp'
    
class OrganizationModule(models.Model):
    organization = models.ForeignKey(Organization, related_name='membership')
    module = models.ForeignKey(Module, related_name='membership') 
    position = models.IntegerField()
    
    class Meta:
        app_label = 'ClientApp'
    
class User(models.Model):
    id = models.AutoField(primary_key=True)
    firstName = models.CharField(max_length=30)
    lastName = models.CharField(max_length=50)
    mail = models.CharField(max_length=50)
    password = models.CharField(max_length=15)
    organization = models.ForeignKey(Organization)
    
    class Meta:
        app_label = 'ClientApp'
    
class Announcement(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=100)
    text = models.CharField(max_length=1000)
    #image = models.ImageField()
    publicationDate = models.DateTimeField()
    organization = models.ForeignKey(Organization)
    
    class Meta:
        app_label = 'ClientApp'
    
class Comment(models.Model):
    id = models.AutoField(primary_key=True)
    text = models.CharField(max_length=300)
    publicationDate = models.DateTimeField()
    user = models.ForeignKey(User)
    
    class Meta:
        app_label = 'ClientApp'
    
