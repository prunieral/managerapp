# -*- coding: utf8 -*-
from django.shortcuts import render
from ClientApp.business.UserBusiness import UserBusiness
from ClientApp.models.forms.LoginForm import LoginForm


def home(request, *args, **kwargs):
    return render(request, "home.html", kwargs.update({"request": request}))