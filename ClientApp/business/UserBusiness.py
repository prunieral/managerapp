# -*- coding: utf8 -*-
from ClientApp.models.models import User
from django.core.exceptions import ObjectDoesNotExist

class UserBusiness():
    
    @staticmethod
    def getAllUsers(self):
        print "UserBusiness.getAllUsers"
        return User.objects.all()
    
    
    @staticmethod
    def isLogCorrect(mail, password):
        print "UserBusiness.isLogCorrect"
        try:
            return User.objects.get(mail=mail, password=password)
        except ObjectDoesNotExist:
            return None
        except Exception, e:
            print e
            
        
    
