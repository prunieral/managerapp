from ClientApp.models.models import Announcement


class AnnouncementBusiness():
    
    
    @staticmethod
    def getAllAnnouncements():
        print "AnnouncementBusiness.getAllAnnouncements"
        return Announcement.objects.all()
    
    @staticmethod
    def getAnnouncementsOfOrganization(organization):
        print "AnnouncementBusiness.getAnnouncementOfOrganization"
        return Announcement.objects.filter(organization=organization)
    
    